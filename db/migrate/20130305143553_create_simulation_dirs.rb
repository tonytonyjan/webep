class CreateSimulationDirs < ActiveRecord::Migration
  def change
    create_table :simulation_dirs do |t|
      t.references :guest
      t.string :sim_dir

      t.timestamps
    end
    add_index :simulation_dirs, :guest_id
  end
end
