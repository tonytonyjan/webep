class DropProductsAttributes < ActiveRecord::Migration
  def up
    drop_table :products_attributes
  end

  def down
    create_table :products_attributes do |t|
      t.references :product
      t.string :key
      t.string :value

      t.timestamps
    end
    add_index :products_attributes, :product_id
  end
end
