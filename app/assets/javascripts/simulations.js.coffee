# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

APP.simulations =
  index: () ->
    $('#select_all').change (e) -> $('.selectable').prop('checked', this.checked)
  new: () ->
    $('#splash_screen').fadeIn 3000, () ->
      $(this).hide()
      $('#splash_screen_container').hide()
      $('#wizard_container').show()
    simulation_available = true
    $('#wizard').smartWizard(
      labelNext: '下一步'
      labelPrevious: '上一步'
      labelFinish: '完成'
      onShowStep: (data) ->
        step = data.attr('rel')
        if step == '5' && simulation_available
          $('form#new_simulation').submit()
          $('#simulating').show()
          $('#report').hide()
          simulation_available = false
    )
    update_simulation_building_model = () ->
      $('#simulation_building_model').val($('#F10_select').val())
    $('#10f-btn').click update_simulation_building_model
    $('#F10_select').change update_simulation_building_model
    $('form#new_simulation').bind 'ajax:success', (event, data) ->
      check = () ->
        $.ajax
          url: data.check_url
          dataType: 'json'
        .done (data) ->
          console.log(data)
          window.location.replace(data.report_url) if data.sim.simulation_dir
        setTimeout(check, 5000)
      check()
    .bind 'ajax:error', (xhr, status, error) ->
      switch status.status
        when 401 then window.location.replace('/users/sign_in')
    svg()
    update_visible() # hide or show
    update_air_conditioner() # set air conditioner select options
    update_cop() # set air conditioner cop

    update_cooling_ability_for_air()

    role = $('#simulation_role')
    $('#role-normal-btn').click () ->
      role.val('normal')
      update_visible()
    $('#role-air-btn').click () ->
      role.val('air')
      update_visible()
    $('#role-light-btn').click () ->
      role.val('light')
      update_visible()

    $([$('#small-office-btn'), $('#3f-btn'), $('#4f-btn')]).each (index, element) ->
      element.click update_building_model

    $('#simulation_weather').change update_map

    $('#air_conditioner_level')
      .change update_air_conditioner

    $('#air_conditioner_type').select2
      width: 'resolve'
      allowClear: true
      ajax:
        url: '/products/attributes'
        dataType: 'json'
        data: (term, page) ->
          term: term
          page: page
          attribute: 'air_conditioner_type'
        results: (data, page) ->
          data
    .change update_air_conditioner

    $('#air_conditioner')
      .change update_cop

    # for air
    $('#air_conditioner_type_for_air').change () ->
      update_cooling_ability_for_air()
    $('#cooling_ability_for_air').change () ->
      update_energy_level_for_air()
    $('#energy_level_for_air').change () ->
      update_cop_for_air()

    # deg
    $('#simulation_deg').after($('#deg_other'))
    $('#deg_other').change (e) ->
      $('#simulation_deg').val($('#deg_other').val())
  total_site_energy_compare: () ->
    ctx = document.getElementById("chart").getContext("2d");
    data = {
      labels: $('#chart').data('labels')
      datasets: [
        {
          fillColor: "rgba(151,187,205,0.5)"
          strokeColor: "rgba(151,187,205,1)"
          data: $('#chart').data('data')
        }
      ]
    }
    options =
      scaleLabel: "<%=value%> kWh"
      scaleOverride: $('#chart').data('scale_override')
      scaleSteps: $('#chart').data('scale_steps')
      scaleStepWidth: $('#chart').data('scale_step_width')
      scaleStartValue: $('#chart').data('scale_start_value')
    new Chart(ctx).Bar data, options

select2_ajax_object = (params = {}) ->
  width: 'resolve'
  formatResult: (object, container, query) ->
    '<div><b>' + object.company + '</b><br/>' +
    object.text + '<br/>' +
    object.air_conditioner_type + '</div>'

  ajax:
    url: '/products/search'
    dataType: 'json'
    data: (term, page) ->
      params.select_options = true
      params.company = term
      params.page = page
      params
    results: (data, page) ->
      data

update_building_model = () ->
  $('#simulation_building_model').val($(this).attr('data-value'))
  update_visible()

update_air_conditioner = () ->
  $('#air_conditioner').select2 select2_ajax_object
    level: $('#air_conditioner_level').val()
    air_conditioner_type: $('#air_conditioner_type').val()
    product_type: 'air_conditioner'
    air_conditioner_cooling_capacity: $('#air_conditioner_cooling_capacity').val()
update_cop = (event) ->
  $('#simulation_cop').val($('#air_conditioner').select2('data')?.cop)
  $('#simulation_air_conditioner_id').val($('#air_conditioner').val())
update_visible = () ->
  role = $('#simulation_role')
  air_vars = $('#air-vars')
  light_vars = $('#light-vars')
  switch role.val()
    when 'normal'
      air_vars.show()
      $('#for-normal').show()
      $('#for-air').hide()
      light_vars.show()
    when 'air'
      air_vars.show()
      $('#for-normal').hide()
      $('#for-air').show()
      light_vars.hide()
    when 'light'
      air_vars.hide()
      light_vars.show()
    else
      console.error 'Unknown role type: ' + role.val()
  
  building_model = $('#simulation_building_model').val()
  switch building_model
    when 'small_office'
      $('.simulation_people').hide()
      $('.simulation_people_density').show()
    when '3f'
      $('.simulation_people').show()
      $('.simulation_people_density').hide()
    when '4f'
      $('.simulation_people').show()
      $('.simulation_people_density').hide()
    else
      console.error 'Unknown building model: ' + building_model
svg = () ->
  ele = document.getElementById('taiwan')
  ele.addEventListener 'load', (event) ->
    doc = ele.getSVGDocument()
    (
      $(place).click () ->
        $('#simulation_weather').val(this.getAttribute('id'))
        update_map()
    ) for place in doc.getElementsByClassName('available')
update_map = () ->
  (
    if place.getAttribute('id') == $('#simulation_weather').val()
      place.setAttribute('class', 'available selected')
    else
      place.setAttribute('class', 'available')
  ) for place in document.getElementById('taiwan').getSVGDocument().getElementsByClassName('available')

update_cooling_ability_for_air = () ->
  switch $('#air_conditioner_type_for_air').val()
    when 'single'
      $('#cooling_ability_for_air').select2
        minimumResultsForSearch: -1
        data: [
          {id: '<2.2', text: '<2.2', cop: [3.3, 3.15, 2.71]},
          {id: '2.2~4.0', text: '2.2~4.0', cop: [3.35, 3.2, 2.77]},
          {id: '4.0~7.1', text: '4.0~7.1', cop: [3.1, 3, 2.6]},
          {id: '7.1~10.0', text: '7.1~10.0', cop: [3.05, 2.95]}
        ]
    when 'separate'
      $('#cooling_ability_for_air').select2
        minimumResultsForSearch: -1
        data: [
          {id: '<4', text: '<4', cop: [3.85, 3.45, 2.97]},
          {id: '4.0~7.1', text: '4.0~7.1', cop: [3.55, 3.2, 2.77]},
          {id: '7.1~70', text: '7.1~70', cop: [3.4, 3.15]}
        ]
    when 'box'
      $('#cooling_ability_for_air').select2
        minimumResultsForSearch: -1
        data: [{id: '<70', text: '<70', cop: [4.8, 4.25]}]
    else
      console.error 'Unknown type: ' + $('#air_conditioner_type_for_air').val()
  update_energy_level_for_air()
update_energy_level_for_air = () ->
  switch $('#air_conditioner_type_for_air').val()
    when 'single'
      $('#energy_level_for_air').select2
        minimumResultsForSearch: -1
        width: 350
        data:[
          {id: 0, text: 'CNS 3615,14464 （98 年 10 月後）'},
          {id: 1, text: 'CNS 3615,14464 （96 年 8 月 - 98 年 10 月）'},
          {id: 2, text: 'CNS 3615 （91 年 1 月 - 96 年 8 月）'}
        ]
    when 'separate'
      $('#energy_level_for_air').select2
        minimumResultsForSearch: -1
        width: 350
        data:[
          {id: 0, text: 'CNS 3615,14464 （98 年 10 月後）'},
          {id: 1, text: 'CNS 3615,14464 （96 年 8 月 - 98 年 10 月）'},
          {id: 2, text: 'CNS 3615 （91 年 1 月 - 96 年 8 月）'}
        ]
    when 'box'
      $('#energy_level_for_air').select2
        minimumResultsForSearch: -1
        width: 350
        data:[
          {id: 0, text: 'CNS 3615,14464 （98 年 10 月後）'},
          {id: 1, text: 'CNS 3615,14464 （96 年 8 月 - 98 年 10 月）'}
        ]
    else
      console.error 'Unknown type: ' + $('#air_conditioner_type_for_air').val()
  update_cop_for_air()

update_cop_for_air = () ->
  id = $('#energy_level_for_air').select2('data')?.id
  cop = $('#cooling_ability_for_air').select2('data')?.cop?[id]
  $('#simulation_cop').val cop