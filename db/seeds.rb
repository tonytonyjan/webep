# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'csv'
air_conditioners = []
CSV.foreach(File.expand_path('product_seeds.csv', __dir__)){|row|
  company = row[0]
  model_number = row[2]
  air_conditioner_type = row[3]
  cooling_capacity = row[4][/[\d\.]+/].to_f
  cop = row[5][/[\d\.]+/].to_f
  level = row[6][/\d+/].to_i
  registration_id = row[9]
  params = {
    company: company,
    name: model_number,
    air_conditioner_type: air_conditioner_type,
    cooling_capacity: cooling_capacity,
    cop: cop,
    level: level,
    registration_id: registration_id
  }
  air_conditioners << params
}
Products::AirConditioner.create! air_conditioners
`rake products:clean_the_old`