class HitCounter
  @@counter_file_path = File.join(Rails.root, 'lib', 'counter.txt')

  def self.increase
    File.open(@@counter_file_path, File::RDWR | File::CREAT, 0644) {|f|
      f.flock(File::LOCK_EX)
      value = f.read.to_i + 1
      f.rewind
      f.write("#{value}\n")
      f.flush
      f.truncate(f.pos)
      value
    }
  end

  def self.read
    if File.exist?(@@counter_file_path)
      File.open(@@counter_file_path, "r") {|f|
        f.flock(File::LOCK_SH)
        f.read.to_i
      }
    else
      0
    end
  end
end