require 'spec_helper'

describe DataAnalysis do
  
  output = Simulation::Output.new(File.join(Rails.root, 'spec', 'datas', 'Output'))
  
  it 'can initialize from string or output object' do
    DataAnalysis.new(output.csv.select{|s| s =~ /Table.csv$/}.first)
    DataAnalysis.new(output)
  end

  it 'accepts string' do
    da = DataAnalysis.new(output)
    da.get_topic("Utility Use Per Conditioned Floor Area").should  eq([[nil, "Electricity Intensity [MJ/m2]", "Natural Gas Intensity [MJ/m2]", "Other Fuel Intensity [MJ/m2]", "District Cooling Intensity [MJ/m2]", "District Heating Intensity [MJ/m2]", "Water Intensity [m3/m2]"], ["Lighting", "51.54", "0.00", "0.00", "0.00", "0.00", "0.00"], ["HVAC", "43.61", "25.06", "0.00", "0.00", "0.00", "0.03"], ["Other", "85.81", "0.00", "0.00", "0.00", "0.00", "0.00"], ["Total", "180.95", "25.06", "0.00", "0.00", "0.00", "0.03"]])
  end

  it 'accepts array of array' do
    da = DataAnalysis.new(output)
    da.get_topic([["REPORT:", "ZoneCoolingSummaryMonthly"], ["FOR:", "CORE_ZN"]]).should eq([[nil, "ZONE/SYS SENSIBLE COOLING ENERGY [J]", "ZONE/SYS SENSIBLE COOLING RATE {Maximum}[W]", "ZONE/SYS SENSIBLE COOLING RATE {TIMESTAMP}", "OUTDOOR DRY BULB {AT MAX/MIN} [C]", "OUTDOOR WET BULB {AT MAX/MIN} [C]", "ZONE TOTAL INTERNAL LATENT GAIN [J]", "ZONE TOTAL INTERNAL LATENT GAIN RATE {Maximum}[W]", "ZONE TOTAL INTERNAL LATENT GAIN RATE {TIMESTAMP}", "OUTDOOR DRY BULB {AT MAX/MIN} [C]", "OUTDOOR WET BULB {AT MAX/MIN} [C]"], ["January", "0.453446E+09", "1304.98", "06-JAN-07:05", "6.30", "5.70", "0.310384E+09", "501.68", "20-JAN-16:00", "20.60", "18.12"], ["February", "0.407146E+09", "937.90", "18-FEB-07:13", "10.03", "8.99", "0.278470E+09", "518.72", "23-FEB-15:09", "23.67", "19.61"], ["March", "0.387692E+09", "964.17", "01-MAR-17:00", "11.80", "9.99", "0.367679E+09", "578.23", "15-MAR-15:00", "28.00", "21.21"], ["April", "79456500.89", "618.00", "11-APR-12:20", "17.00", "16.38", "0.370927E+09", "577.84", "25-APR-13:20", "29.87", "24.37"], ["May", "57031632.41", "510.45", "16-MAY-10:00", "17.00", "17.00", "0.420807E+09", "578.02", "11-MAY-13:20", "24.77", "20.31"], ["June", "63756076.35", "466.94", "30-JUN-16:00", "34.00", "27.27", "0.442792E+09", "577.89", "23-JUN-07:19", "29.67", "25.94"], ["July", "0.180162E+09", "735.20", "21-JUL-15:45", "37.17", "27.34", "0.417452E+09", "585.26", "20-JUL-14:09", "34.83", "28.75"], ["August", "0.128888E+09", "532.57", "22-AUG-16:00", "36.00", "25.14", "0.471338E+09", "577.97", "18-AUG-07:00", "28.00", "25.49"], ["September", "33119397.24", "416.53", "14-SEP-15:53", "33.00", "27.69", "0.411629E+09", "578.34", "19-SEP-08:20", "27.03", "24.86"], ["October", "13647766.82", "275.97", "25-OCT-21:00", "18.00", "14.99", "0.406906E+09", "578.68", "20-OCT-12:00", "25.00", "20.81"], ["November", "0.103390E+09", "1292.62", "29-NOV-12:54", "10.00", "10.00", "0.382194E+09", "578.17", "22-NOV-14:00", "27.00", "22.77"], ["December", "0.305376E+09", "1049.10", "21-DEC-16:00", "11.00", "8.41", "0.315197E+09", "545.69", "08-DEC-13:20", "27.50", "19.35"], [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil], ["Annual Sum or Average", "0.221311E+10", nil, nil, nil, nil, "0.459577E+10", nil, nil, nil, nil], ["Minimum of Months", "13647766.82", "275.97", nil, "6.30", "5.70", "0.278470E+09", "501.68", nil, "20.60", "18.12"], ["Maximum of Months", "0.453446E+09", "1304.98", nil, "37.17", "27.69", "0.471338E+09", "585.26", nil, "34.83", "28.75"]])
  end
end
