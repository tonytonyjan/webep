require 'nokogiri'
require 'open-uri'
child_doc = nil
count = 1
(1..951).each{|page_num|
  url = "https://ranking.energylabel.org.tw/_outweb/product/Approval/list.asp?Class=1&pageno=#{page_num}"
  doc = Nokogiri::HTML(open(url))
  doc.css('a.grida').each{|link|
    child_url = URI.join(url, link[:href])
    child_doc = Nokogiri::HTML(open(child_url))
    filename = File.expand_path("../tmp/pages/#{count}.html", __dir__)
    File.write(filename, child_doc.to_s)
    count += 1
    puts "Saved as #{filename}"
  }
}