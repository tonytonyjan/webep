class AddSomeAirConditionerAttributesToProducts < ActiveRecord::Migration
  def change
    add_column :products, :cop, :float
    add_column :products, :air_conditioner_type2, :string
    add_column :products, :cooling_capacity, :float
  end
end
