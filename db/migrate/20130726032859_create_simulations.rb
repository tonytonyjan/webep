class CreateSimulations < ActiveRecord::Migration
  def change
    create_table :simulations do |t|
      t.string :role
      t.string :building_model
      t.string :weather
      t.float :room_temperature
      t.float :cop
      t.integer :rate_of_decline
      t.string :power
      t.string :sun_light
      t.float :people_density
      t.integer :people
      t.string :simulation_dir
      t.references :user
      t.references :guest
      t.string :status

      t.timestamps
    end
    add_index :simulations, :user_id
    add_index :simulations, :guest_id
  end
end
