namespace :simulation do

  def run_simulate simulations
    simulations.each{|item|
      # OOXX.idf
      basename = File.basename(item[:sim].input_file_path, '.erb')
      # tmp/simualtion/
      simulation_dir = Rails.root.join('tmp', 'simulation')
      FileUtils.mkdir_p simulation_dir

      # tmp/simulation/OOXX.idf.erbyyyymmdd/
      tmp_dir = File.join(simulation_dir, item[:name])
      Dir.mkdir(tmp_dir)
      # tmp/simulation/OOXX.idf.erbyyyymmdd/OOXX.idf
      tmp_file_path = File.join(tmp_dir, basename)
      # render erb file
      File.write(tmp_file_path, item[:sim].erb_result)
      p Simulation::EP_BIN, tmp_file_path, item[:sim].weather_file_path, '='*100
      `#{Simulation::EP_BIN} '#{tmp_file_path}' '#{item[:sim].weather_file_path}'`
    }
  end

  task :simulate4 => :environment do
    simulations = []
    cops = [3.25, 3.1, 2.9, 2.8]
    temperatures = [22, 23, 24, 25, 26, 27, 28, 29]
    cops.each{|cop|
      temperatures.each{|temperature|
        simulations << {
          name: "T_#{temperature}-COP_#{cop}",
          sim: Simulation.new(
            building_model: '3f', weather: 'taipei',
            room_temperature: temperature, cop: cop, rate_of_decline: 100.0
          )
        }
      }
    }
    run_simulate simulations
  end

  task :simulate3 => :environment do
    simulations = []
    # case 1
    cops = [3.4, 3.25, 3.1, 2.9, 2.8]
    cops.each_with_index{|cop, index|
      simulations << {
        name: "T_26-COP_#{cop}",
        sim: Simulation.new(
          building_model: '3f', weather: 'taipei',
          room_temperature: 26, cop: cop, rate_of_decline: 100.0
        )
      }
    }
    # case 2
    temperatures = [22, 23, 24, 25, 27, 28, 29]
    temperatures.each_with_index{|temperature, index|
      simulations << {
        name: "T_#{temperature}-COP_3.4",
        sim: Simulation.new(
          building_model: '3f', weather: 'taipei',
          room_temperature: temperature, cop: 3.4, rate_of_decline: 100.0
        )
      }
    }
    # case 3
    cops = [4.17, 3.93, 3.69, 3.45, 3.21, 3.87, 3.65, 3.42, 3.2, 2.98, 3.81, 3.59, 3.37, 3.15, 2.93]
    cops.each_with_index{|cop, index|
      simulations << {
        name: "T_26-COP_#{cop}",
        sim: Simulation.new(
          building_model: '3f', weather: 'taipei',
          room_temperature: 26, cop: cop, rate_of_decline: 100.0
        )
      }
    }

    run_simulate simulations
  end
  
  task :simulate2 => :environment do
    cops = [[3.87, '詮宏空調-4TTK514W11EF'], [3.65, '太欣科技-TXA-631RE'], [3.37, '盟坤企業-FU-130CSD1(G)'], [3.15, '金大阪國際-SRO-100F'], [2.73, '雅光-RA-45Y2']]
    simulations = []
    cops.each_with_index{|cop, index|
      simulations << {
        name: "#{index + 1}級-#{cop[1]}",
        sim: Simulation.new(
          building_model: '3f', weather: 'hsinchu',
          room_temperature: 26, cop: cop[0],
        )
      }
    }
    run_simulate simulations
  end

  task :simulate => :environment do
    room_temperatures = [24, 26, 28]
    d_cop = [[3.3, '<2.2'], [3.35, '2.2-2.4'], [3.1, '4.0-7.1'], [3.05, '7.1-10']]
    f_cop = [[3.85, '<4'], [3.55, '4-7.1'], [3.4, '7.1-70']]

    simulations = []
    room_temperatures.each{|t|
      d_cop.each{|cop|
        simulations << {
          name: "#{t}-單體-#{cop[1]}",
          sim: Simulation.new(
            building_model: '3f', weather: 'hsinchu',
            room_temperature: t, cop: [24,28].include?(t) ? 3.1 : cop[0]
          )
        }
      }
      f_cop.each{|cop|
        simulations << {
          name: "#{t}-分離-#{cop[1]}",
          sim: Simulation.new(
            building_model: '3f', weather: 'hsinchu',
            room_temperature: t, cop: [24,28].include?(t) ? 3.1 : cop[0]
          )
        }
      }
    }
    run_simulate simulations
  end
end
