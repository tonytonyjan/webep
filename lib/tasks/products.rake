namespace :products do
  desc "Clean the old prlducts with the same name"
  task :clean_the_old => :environment do
    Products::AirConditioner.group(:name).having('COUNT(*) > 1').each{|i|
      begin
        products = Products::AirConditioner.where(name: i.name)
        neweast = products.max_by(&:registration_year)
        products - [neweast]
        products.destroy_all
      rescue
        puts $!, $@
      end
    }
  end
end
