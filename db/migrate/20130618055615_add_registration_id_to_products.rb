class AddRegistrationIdToProducts < ActiveRecord::Migration
  def change
    add_column :products, :registration_id, :string
  end
end
