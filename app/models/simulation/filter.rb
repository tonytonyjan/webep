class Simulation::Filter
  extend ActiveModel::Naming
  include ActiveModel::Conversion

  attr_accessor :companies, :weathers, :building_models

  def initialize(attributes = {})
    attributes ||= {}
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def search_result
    sims = Simulation.scoped
    sims = sims.includes(:air_conditioner).where(products: {company: companies}) if companies.is_a?(Array) && companies.reject!(&:blank?).present?
    sims = sims.where weather: weathers if weathers.is_a?(Array) && weathers.reject!(&:blank?).present?
    sims = sims.where building_model: building_models if building_models.is_a?(Array) && building_models.reject!(&:blank?).present?
    sims
  end

  def persisted?
    false
  end
end
