# == Schema Information
#
# Table name: simulation_dirs
#
#  id         :integer          not null, primary key
#  guest_id   :integer
#  sim_dir    :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SimulationDir < ActiveRecord::Base
  belongs_to :guest
  attr_accessible :sim_dir

  def sim_output
    Simulation::Output.new File.join(sim_dir, 'Output')
  end
end
