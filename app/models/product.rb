# == Schema Information
#
# Table name: products
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  level                :integer
#  product_type         :string(255)
#  company              :string(255)
#  cop                  :float
#  air_conditioner_type :string(255)
#  cooling_capacity     :float
#  registration_id      :string(255)
#

class Product < ActiveRecord::Base
  attr_accessible :name, :company, :level, :product_type, :registration_id

  validates :name, :level, :company, presence: true
  validates :level, inclusion: {in: (1..5)}
  PRODUCT_TYPES = %w(air_conditioner lamp)
  validates :product_type, presence: true, inclusion: PRODUCT_TYPES

  def self.search(params = {})
    products = Product.scoped
    products = products.where('company LIKE ?', "%#{params[:company]}%") if params[:company].present?
    %w(level product_type air_conditioner_type).each{|attribute|
      products = products.where(attribute => params[attribute]) if params[attribute].present?
    }
    if params['air_conditioner_cooling_capacity'].present?
      range = case params['air_conditioner_cooling_capacity']
              when "1~2" then (1.0...2.0)
              when "2~3" then (2.0...3.0)
              when "3~4" then (3.0...4.0)
              end
      products = products.where(cooling_capacity: range) if range
    end
    products
  end

  def lamp?
    product_type == 'lamp'
  end

  def air_conditioner?
    product_type == 'air_conditioner'
  end

  def downcast
    case product_type
    when 'air_conditioner' then becomes(Products::AirConditioner)
    when 'lamp' then becomes(Products::Lamp)
    else self
    end
  end

  def registration_year
    registration_id[/\w+-(\d+)-\d+/, 1]
  end
end
