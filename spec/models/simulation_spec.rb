# == Schema Information
#
# Table name: simulations
#
#  id                 :integer          not null, primary key
#  role               :string(255)
#  building_model     :string(255)
#  weather            :string(255)
#  room_temperature   :float
#  cop                :float
#  rate_of_decline    :integer
#  power              :string(255)
#  sun_light          :string(255)
#  people_density     :float
#  people             :integer
#  simulation_dir     :string(255)
#  user_id            :integer
#  guest_id           :integer
#  status             :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  deg                :float            default(0.0), not null
#  air_conditioner_id :integer
#

require 'spec_helper'

describe Simulation do
 before :all do
    @simulation = Simulation.new(
      role: Simulation::Role::NORMAL,
      building_model: 'minimal',
      weather: Simulation::Weather::TAIPEI,
      room_temperature: 20,
      cop: 3.0,
      power: 4,
      sun_light: 100,
      people_density: 10
      people: 2
    )
  end
  
  it 'works' do
    output = nil
    output = @simulation.run
    output.should.respond_to? :html
  end

  it 'delays' do
    job = Simulation::SimulationJob.new(@simulation)
    Delayed::Job.enqueue job
  end
end
