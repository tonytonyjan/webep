# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

APP.products =
  new: () ->
    form_init()
  create: () ->
    form_init()
  edit: () ->
    form_init()
  update: () ->
    form_init()

form_init = (params = {}) ->
  $('#product_company').select2 select2_params(attribute: 'company')
  $('#product_air_conditioner_type').select2 select2_params(attribute: 'air_conditioner_type')
select2_params = (params = {}) ->
  width: 'resolve'
  allowClear: true
  ajax:
    url: '/products/attributes'
    dataType: 'json'
    data: (term, page) ->
      term: term
      page: page
      attribute: params.attribute
    results: (data, page) ->
      data
  initSelection: (element, callback) ->
    data = id: element.val(), text: element.val()
    callback data
  createSearchChoice: (term) ->
    return {id: term, text: term}