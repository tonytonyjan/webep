require 'nokogiri'
require 'csv'
CSV.open(File.expand_path('../db/product_seeds.csv', __dir__), 'wb'){|csv|
  Dir.glob(File.expand_path("../tmp/pages/*.html", __dir__)).each{|path|
    doc = Nokogiri::HTML(File.new(path))
    csv << doc.css('th.gridth').map{|node|
      node.next.next.text.strip
    }
  }
}