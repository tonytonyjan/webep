class RenameAirConditionerType2ToAirConditionerType < ActiveRecord::Migration
  def change
    rename_column :products, :air_conditioner_type2, :air_conditioner_type
  end
end
