class AddAirConditionerTypeToProducts < ActiveRecord::Migration
  def change
    add_column :products, :air_conditioner_type, :string
  end
end
