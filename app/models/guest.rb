# == Schema Information
#
# Table name: guests
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Guest < ActiveRecord::Base
  # attr_accessible :title, :body
  has_many :simulations, uniq: true
end
