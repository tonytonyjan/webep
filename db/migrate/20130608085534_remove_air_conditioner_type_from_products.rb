class RemoveAirConditionerTypeFromProducts < ActiveRecord::Migration
  def up
    remove_column :products, :air_conditioner_type
  end

  def down
    add_column :products, :air_conditioner_type, :string
  end
end
