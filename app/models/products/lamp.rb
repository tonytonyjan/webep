# == Schema Information
#
# Table name: products
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  level                :integer
#  product_type         :string(255)
#  company              :string(255)
#  cop                  :float
#  air_conditioner_type :string(255)
#  cooling_capacity     :float
#  registration_id      :string(255)
#

class Products::Lamp < Product
  default_scope where(product_type: :lamp)
  after_initialize{|x| x.product_type = 'lamp'}
  validates :product_type, inclusion: %w(lamp)
end
