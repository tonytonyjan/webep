# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130912002948) do

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "guests", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "products", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.integer  "level"
    t.string   "product_type"
    t.string   "company"
    t.float    "cop"
    t.string   "air_conditioner_type"
    t.float    "cooling_capacity"
    t.string   "registration_id"
  end

  create_table "simulation_dirs", :force => true do |t|
    t.integer  "guest_id"
    t.string   "sim_dir"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "simulation_dirs", ["guest_id"], :name => "index_simulation_dirs_on_guest_id"

  create_table "simulations", :force => true do |t|
    t.string   "role"
    t.string   "building_model"
    t.string   "weather"
    t.float    "room_temperature"
    t.float    "cop"
    t.integer  "rate_of_decline"
    t.string   "power"
    t.string   "sun_light"
    t.float    "people_density"
    t.integer  "people"
    t.string   "simulation_dir"
    t.integer  "user_id"
    t.integer  "guest_id"
    t.string   "status"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.float    "deg",                :default => 0.0, :null => false
    t.integer  "air_conditioner_id"
  end

  add_index "simulations", ["air_conditioner_id"], :name => "index_simulations_on_air_conditioner_id"
  add_index "simulations", ["guest_id"], :name => "index_simulations_on_guest_id"
  add_index "simulations", ["user_id"], :name => "index_simulations_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.boolean  "is_admin",               :default => false
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
