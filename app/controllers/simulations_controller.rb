class SimulationsController < ApplicationController
  before_filter :authenticate_user!, except: [:new]
  before_filter :set_sim, only: [:simulating, :show, :check]
  authorize_resource instance_name: :sim, except: [:new, :create]

  def index
    @filter = Simulation::Filter.new(params[:simulation_filter])
    @companies = Products::AirConditioner.select(:company).group(:company).map(&:company)
    @sims = @filter.search_result.accessible_by(current_ability).order('simulations.id DESC').page(params[:page])
  end

  def new
    @hit_count = HitCounter.increase
    @sim = Simulation.new
  end

  def create
    user = current_user || current_guest
    if @sim = user.simulations.create(params[:simulation])
      if same_sim = @sim.find_the_same
        @sim.update_attribute :simulation_dir, same_sim.simulation_dir
      else
        job = Simulation::SimulationJob.new(@sim)
        Delayed::Job.enqueue job
      end
      respond_to{|format|
        format.html{redirect_to simulating_simulation_path(@sim)}
        format.json{render json: {check_url: check_simulation_url(@sim)}}
      }
    else
    end
  end

  def simulating
    if @sim.simulation_dir
      redirect_to @sim
    end
  end

  def show
    if @sim.finished?
      begin
        @data = DataAnalysis.new @sim.output
        params[:no_layout] ? render(layout: nil) : render
      rescue
        Rails.logger.warn $!
        Rails.logger.debug $@.join($/)
        render file: @sim.output.html
      end
    else
      redirect_to simulating_simulation_path(@sim)
    end
  end

  def total_site_energy_compare
    @scale_override = (params[:start_value].present? && params[:end_value].present?).to_s
    @start_value = params[:start_value].presence && params[:start_value].to_i.abs
    @end_value = params[:start_value].presence && params[:end_value].to_i.abs
    @scale_steps = 10
    @scale_step_width = (@end_value.to_i - @start_value.to_i) / @scale_steps
    @scale_start_value = @start_value
    @simulations = Simulation.where(id: params[:simulation_ids]).select(&:finished?)
    @datas = DataAnalysis.retrieve_total_site_energies(@simulations)
  end

  # ajax
  def check
    render json: {sim: @sim, report_url: simulation_url(@sim)}
  end

  private
  def set_sim
    user = current_user || current_guest
    @sim = user.simulations.find_by_id(params[:id]) || user.simulations.last
    redirect_to root_path unless @sim
  end
end
