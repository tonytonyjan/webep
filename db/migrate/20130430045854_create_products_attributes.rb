class CreateProductsAttributes < ActiveRecord::Migration
  def change
    create_table :products_attributes do |t|
      t.references :product
      t.string :key
      t.string :value

      t.timestamps
    end
    add_index :products_attributes, :product_id
  end
end
