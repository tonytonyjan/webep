require 'spec_helper'

describe HitCounter do
  it 'works' do
    HitCounter.increase.is_a?(Integer).should eq(true)
    HitCounter.read.is_a?(Integer).should eq(true)
  end
end
