class ProductsController < ApplicationController
  http_basic_authenticate_with name: 'admin', password: 'ej/ u06m04', except: %i(search attributes)
  before_filter :all_product, only: [:index]
  before_filter :new_product, only: [:new, :create]
  before_filter :set_product, only: [:show, :edit, :update, :destroy]

  # GET /products
  # GET /products.json
  def index
    @products = model_class.where(id: params[:ids].split(',')) if params[:ids].present?
    respond_to do |format|
      format.html # index.html.erb
      format.json do
        # used in select2
        render json: @products.map{ |x| {id: x.id, text: x.name} }
      end
    end
  end

  # GET /products/1
  # GET /products/1.json
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @product }
    end
  end

  # GET /products/new
  # GET /products/new.json
  def new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @product }
    end
  end

  # GET /products/1/edit
  def edit
  end

  # POST /products
  # POST /products.json
  def create
    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: '新增成功' }
        format.json { render json: @product, status: :created, location: @product }
      else
        format.html { render action: "new" }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /products/1
  # PUT /products/1.json
  def update

    respond_to do |format|
      if @product.update_attributes(params[:product])
        format.html { redirect_to @product, notice: '更新成功' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    flash[:notice] = '已刪除'
    respond_to do |format|
      format.html { redirect_to products_url }
      format.json { head :no_content }
    end
  end

  # ajax for select2
  def search
    results = Product.search(params).page(params[:page]).per(10)
    results.map!{|x|
      {
        id: x.id,
        text: x.name,
        cop: '%.2f' % x.cop,
        company: x.company,
        air_conditioner_type: x.air_conditioner_type
      }
    } if [:select_options].present?
    render json: {
      results: results,
      more: params[:page].to_i < results.num_pages
    }
  end

  # ajax for select2
  def attributes
    attribute_name = params[:attribute]
    raise "attribute must be in #{Product.attribute_names}" unless Product.attribute_names.include?(attribute_name)
    results = Product.select(attribute_name).group(attribute_name)
    results = results.where("#{attribute_name} LIKE ?", "%#{params[:term]}%") if params[:term].present?
    results = results.page(params[:page]).per(10)
    results.map!{|x| {id: x.send(attribute_name), text: x.send(attribute_name)}}
    render json: {
      results: results,
      more: params[:page].to_i < results.num_pages
    }
  end

  private

  def model_class
    params[:product_type].blank? ? Product : params[:product_type].constantize
  end

  def all_product
    @products = model_class.page(params[:page]).per(10)
  end

  def new_product
    @product = model_class.new(params[:product])
  end

  def set_product
    @product = model_class.find(params[:id])
  end
end
