require 'csv'
class DataAnalysis
  def initialize table_file_path_or_output_object
    @rows = case table_file_path_or_output_object.class.name
    when "String"
      CSV.read(table_file_path_or_output_object)
    when "Simulation::Output"
      CSV.read(table_file_path_or_output_object.csv.select{|s| s =~ /Table.csv$/}.first)
    else
      raise "The variable is neither Sting nor Simulation::Output"
    end
  end

  def self.retrieve_total_site_energies sim_outputs
    sim_outputs = sim_outputs.map(&:output) if sim_outputs.first.is_a?(Simulation)
    datas = sim_outputs.map{|output| DataAnalysis.new(output)}
    total_energies = datas.map{|data| data.get_topic('Site and Source Energy')[1][1].to_f}
  end

  def get_topic name_or_array # ex. "Utility Use Per Conditioned Floor Area"
    result = []
    state = :not_found
    i = 0
    @rows.each{ |row|
      case state
      when :not_found
        if row.first == name_or_array || row == name_or_array
          state = :find_nil
        elsif row == name_or_array.first
          state = :find_next
        end
      when :find_next
        i += 1
        if row == name_or_array[i]
          state = :find_nil
        else
          i = 0
          state = :not_found
        end
      when :find_nil
        if !row.empty? && row.first == nil
          result << row[1..-1]
          state = :found_nil
        end
      when :found_nil
        break if row.empty?
        result << row[1..-1]
      end
    }
    result
  end
end