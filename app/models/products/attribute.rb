class Products::Attribute < ActiveRecord::Base
  belongs_to :product
  attr_accessible :key, :value
  validates :key, :value, presence: true

  def self.search params = {}
    attributes = Products::Attribute.scoped.joins(:product)
    attributes = attributes.where('key LIKE ?', "%#{params[:term]}%") if params[:term].present?
    attributes = attributes.where('products.product_type' => params[:type]) if params[:type].present?
    attributes = attributes.group(:key)
    attributes = attributes.map{|x| {id: x.key, text: x.key}}
  end
end
