class AddDegToSimulations < ActiveRecord::Migration
  def change
    add_column :simulations, :deg, :float, null: false, default: 0.0
  end
end
