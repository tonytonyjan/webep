class AddAirConditionerIdToSimulations < ActiveRecord::Migration
  def change
    add_column :simulations, :air_conditioner_id, :integer
    add_index :simulations, :air_conditioner_id
  end
end
