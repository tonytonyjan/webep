class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :get_hit_count

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, alert: exception.message
  end

  def current_guest
    unless @guest = Guest.find_by_id(session[:guest_id])
      @guest = Guest.create
      session[:guest_id] = @guest.id
    end
    @guest
  end

  def get_hit_count
    @hit_count = HitCounter.read
  end
end
