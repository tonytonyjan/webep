# encoding: utf-8
# == Schema Information
#
# Table name: simulations
#
#  id                 :integer          not null, primary key
#  role               :string(255)
#  building_model     :string(255)
#  weather            :string(255)
#  room_temperature   :float
#  cop                :float
#  rate_of_decline    :integer
#  power              :string(255)
#  sun_light          :string(255)
#  people_density     :float
#  people             :integer
#  simulation_dir     :string(255)
#  user_id            :integer
#  guest_id           :integer
#  status             :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  deg                :float            default(0.0), not null
#  air_conditioner_id :integer
#

class Simulation < ActiveRecord::Base
  BUILDING_MODEL_OPTIONS = [%i(三樓 3f), %i(四樓 4f), %i(小型辦公室 small_office)]
  belongs_to :user
  belongs_to :guest
  belongs_to :air_conditioner, class_name: Products::AirConditioner
  attr_accessible :role, :building_model, :weather,
                :room_temperature, :cop, :rate_of_decline, # cooling system
                :power, :sun_light, # lighting system
                :people_density, # for business building
                :people, # for normal building
                :simulation_dir, :deg,
                :air_conditioner_id
  attr_accessor :weather_file_path, :input_file_path

  EP_BIN = Rails.root.join("lib", "EnergyPlus-7-2-0", "bin", "runenergyplus")
  BuildingModels = {
    'small_office' => Rails.root.join('lib', 'EnergyPlus-7-2-0', 'examples', 'RefBldgSmallOfficeNew2004_for_Sensitivity20130226.imf.erb'),
    '3f' => Rails.root.join('lib', 'EnergyPlus-7-2-0', 'examples', 'RRB_3F_1020731.imf.erb'),
    '4f' => Rails.root.join('lib', 'EnergyPlus-7-2-0', 'examples', '4F-3mm_0912.imf.erb'),
    'minimal' => Rails.root.join('lib', 'EnergyPlus-7-2-0', 'examples', 'Minimal.idf.erb'),
    'top' => Rails.root.join('lib', 'EnergyPlus-7-2-0', 'examples', 'Top_F_3mm_1105.imf.erb'),
    'mid' => Rails.root.join('lib', 'EnergyPlus-7-2-0', 'examples', 'Mid_F_3mm_1105.imf.erb'),
    'btn' => Rails.root.join('lib', 'EnergyPlus-7-2-0', 'examples', 'Bottom_3mm_1105.imf.erb')
  }

  after_initialize do |sim|
    sim.cop = sim.cop.to_f if sim.cop.present?
    sim.rate_of_decline = sim.rate_of_decline.to_i if sim.rate_of_decline.present?

    sim.input_file_path = BuildingModels[building_model] || BuildingModels['small_office']
    sim.weather_file_path = case weather
    when Weather::TAIPEI, Weather::KEELUNG then Rails.root.join('lib', 'EnergyPlus-7-2-0', 'WeatherData', 'TWN_Taipei.466960_IWEC.epw')
    when Weather::HSINCHU, Weather::HSINCHUCITY, Weather::MIAOLI then Rails.root.join('lib', 'EnergyPlus-7-2-0', 'WeatherData', 'Hsinchu2012DOE2.epw')
    when Weather::TAICHUNG then Rails.root.join('lib', 'EnergyPlus-7-2-0', 'WeatherData', 'Taichung.epw')
    when Weather::CHIAYI, Weather::YUNLIN then Rails.root.join('lib', 'EnergyPlus-7-2-0', 'WeatherData', 'ChiaYi_RCKU_48000_TMY3.epw')
    when Weather::TAINAN, Weather::TAINANCITY then Rails.root.join('lib', 'EnergyPlus-7-2-0', 'WeatherData', 'TaiNan.epw')
    when Weather::KAOHSIUNG, Weather::PINGTUNG, Weather::KAOHSIUNGCITY then Rails.root.join('lib', 'EnergyPlus-7-2-0', 'WeatherData', 'Kaohsiung_RCAY_48000_TMY3.epw')
    when Weather::TAITONG then Rails.root.join('lib', 'EnergyPlus-7-2-0', 'WeatherData', 'TaiTong.epw')
    when Weather::HUALIAN, Weather::YILAN then Rails.root.join('lib', 'EnergyPlus-7-2-0', 'WeatherData', 'HuaLian.epw')
    when Weather::TAOYUAN then Rails.root.join('lib', 'EnergyPlus-7-2-0', 'WeatherData', 'TaoYuan.epw')
    when Weather::CHANGHUA, Weather::NANTOU then Rails.root.join('lib', 'EnergyPlus-7-2-0', 'WeatherData', 'ChangHua_TM3.epw')
    else Rails.root.join('lib', 'EnergyPlus-7-2-0', 'WeatherData', 'TWN_Taipei.466960_IWEC.epw')
    end

  end

  def run
    # OOXX.idf
    basename = File.basename(@input_file_path, '.erb')

    # public/simualtion/
    simulation_base_dir = Rails.root.join('public', 'simulation')
    FileUtils.mkdir_p simulation_base_dir
    
    # public/simulation/OOXX.idf.erbyyyymmdd/
    _simulation_dir = Dir.mktmpdir(basename, simulation_base_dir)
    # public/simulation/OOXX.idf.erbyyyymmdd/OOXX.idf
    simulation_file_path = File.join(_simulation_dir, basename)
    # render erb file
    File.write(simulation_file_path, erb_result)

    # run EnergyPlus
    `#{EP_BIN} '#{simulation_file_path}' '#{@weather_file_path}'`

    if $? == 0 # exists correctly
      Output.new File.join(_simulation_dir, 'Output')
      update_attribute :simulation_dir, _simulation_dir
    else
      raise 'Failed to execute EnergyPlus.'
    end
  end

  # Support templating of member data.
  def get_binding
    binding
  end

  def erb_result
    erb = ERB.new(File.read(@input_file_path))
    erb.result(binding)
  end

  def output
    Output.new File.join(simulation_dir, 'Output')
  end

  def building_model_text
    case building_model
    when 'small_office' then '商業類/小型辦公室'
    when '3f' then '住宅類/三樓民宅'
    when '4f' then '住宅類/四樓民宅'
    when 'top' then '住宅類/公寓頂樓'
    when 'mid' then '住宅類/公寓中間樓層'
    when 'btn' then '住宅類/公寓一樓'
    end
  end

  def room_space_text
    case building_model
    when 'small_office' then '100 坪/三房兩廳一衛'
    when '3f' then '100 坪/三房兩廳一衛'
    when 'top' then ''
    when 'mid' then ''
    when 'btn' then ''
    end
  end

  def room_space_ratial_text
    case building_model
    when 'small_office' then '廳:廳:房:房:房:衛 = 4:2:2:1.5:1:1'
    when '3f' then '廳:廳:房:房:房:衛 = 4:2:2:1.5:1:1'
    when 'top' then ''
    when 'mid' then ''
    when 'btn' then ''
    end
  end

  def weather_text
    case weather
    when Weather::TAIPEI then '台北'
    when Weather::HSINCHU then '新竹'
    when Weather::HSINCHUCITY then '新竹市'
    when Weather::TAICHUNG then '台中'
    when Weather::CHIAYI then '嘉義'
    when Weather::TAINAN then '台南'
    when Weather::KAOHSIUNG then '高雄'
    when Weather::TAITONG then '台東'
    when Weather::HUALIAN then '花蓮'
    when Weather::TAOYUAN then '桃園'
    when Weather::CHANGHUA then '彰化'
    when Weather::KEELUNG then '基隆'
    when Weather::YILAN then '宜蘭'
    when Weather::MIAOLI then '苗栗'
    when Weather::YUNLIN then '雲林'
    when Weather::NANTOU then '南投'
    when Weather::PINGTUNG then '屏東'
    when Weather::KAOHSIUNGCITY then '高雄市'
    when Weather::TAINANCITY then '台南市'
    end
  end

  def finished?
    simulation_dir.present?
  end

  def find_the_same
    Simulation.where(
      building_model: building_model,
      weather: weather,
      room_temperature: room_temperature,
      cop: cop,
      rate_of_decline: rate_of_decline,
      power: power,
      people: people,
      deg: deg
    ).where('id IS NOT ? AND simulation_dir IS NOT NULL', id).first
  end

  module Role
    NORMAL, AIR, LIGHT = %w(normal air light)
    OPTIONS = [
      ['一般使用者', NORMAL],
      ['空調業者', AIR],
      ['照明業者', LIGHT]
    ]
  end

  module Weather
    KEELUNG, TAIPEI, TAOYUAN, HSINCHU, HSINCHUCITY, MIAOLI, TAICHUNG, CHANGHUA, YUNLIN, CHIAYI, NANTOU, TAINAN, TAINANCITY, KAOHSIUNG, KAOHSIUNGCITY, PINGTUNG, TAITONG, HUALIAN, YILAN = %w(
    keelung  taipei  taoyuan  hsinchu  hsinchucity  miaoli  taichung  changhua  yunlin  chiayi  nantou  tainan  tainancity  kaohsiung  kaohsiungcity  pingtung  taitong  hualian  yilan)
    OPTIONS = [
      ['基隆', KEELUNG],
      ['台北', TAIPEI],
      ['桃園', TAOYUAN],
      ['新竹', HSINCHU],
      ['新竹市', HSINCHUCITY],
      ['苗栗', MIAOLI],
      ['台中', TAICHUNG],
      ['彰化', CHANGHUA],
      ['雲林', YUNLIN],
      ['嘉義', CHIAYI],
      ['南投', NANTOU],
      ['台南', TAINAN],
      ['台南市', TAINANCITY],
      ['高雄', KAOHSIUNG],
      ['高雄市', KAOHSIUNGCITY],
      ['屏東', PINGTUNG],
      ['台東', TAITONG],
      ['花蓮', HUALIAN],
      ['宜蘭', YILAN]
    ]
  end

  class Output
    def initialize output_dir
      @extensions = {}
      Dir[File.join(output_dir, '*')].each{|file_path|
        extname = File.extname(file_path)[1..-1]
        @extensions[extname] ||= []
        @extensions[extname] << file_path
      }
    end

    def method_missing(method_id)
      str = method_id.id2name
      if @extensions[str]
        @extensions[str].size == 1 ? @extensions[str][0] : @extensions[str]
      else
        super
      end
    end
  end

  class SimulationJob < Struct.new(:simulation)
    def perform
      simulation.run
    end

    def success(job)
    end
  end
end
