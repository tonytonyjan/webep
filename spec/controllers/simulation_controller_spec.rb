require 'spec_helper'

describe SimulationController do

  describe "GET 'wizard'" do
    it "returns http success" do
      get 'wizard'
      response.should be_success
    end
  end

  describe "GET 'report'" do
    it "returns http success" do
      get 'report'
      response.should be_success
    end
  end

end
